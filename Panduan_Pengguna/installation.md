# Panduan Pemasangan Cablaka

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Siapkan Server

Cablaka merupakan aplikasi berbasis web sehingga kita perlu mempersiapkan server web yang digunakan untuk menjalankan Cablaka. Spesifikasi server yang dibutuhkan Cablaka sebenarnya relatif ringan dan menjadi salah satu pertimbangan penting dalam membuat sistem Cablaka.

Kami mempertimbangkan agar Cablaka dapat bekerja pada server dengan spesifikasi yang relatif ringan dikarenakan kami mengingat bahwa tidak setiap desa mampu menyediakan infrastruktur yang ideal. Bagi beberapa desa, memiliki satu buah komputer bisa saja disebut sebagai sebuah kemewahan tersendiri. Oleh karena itu, kami merancang agar Cablaka dapat berjalan pada laptop sekalipun.

Cablaka sendiri hanya membutuhkan komputer/laptop dengan spesifikasi yang dapat menjalankan layanan server sebagai berikut.

* Web server dengan rekomendasi kami adalah NGINX atau Apache httpd.
* Database server dengan rekomendasi kami adalah MariaDB atau MySQL.
* PHP dengan versi minimal 7.3.

> Anda dapat berkonsultasi perihal kebutuhan infrastruktur Cablaka kepada tim kami.

## Persiapkan API Mapbox

Cablaka menggunakan peta dari OpenStreetMap dengan fasilitas yang disediakan oleh Mapbox. Untuk itu, kita harus memiliki API dari Mapbox terlebih dahulu jika ingin memanfaatkan fitur peta yang ada di Cablaka.

> Tim kami siap membantu dalam mengurus kebutuhan API Mapbox. Silakan hubungi tim pemasangan Anda.

## Persiapkan Data

Setelah pemasangan Cablaka pada komputer yang sudah disediakan, sebenarnya kita sudah dapat menggunakan Cablaka. Tetapi, kita belum memiliki data-data yang dibutuhkan untuk operasional layanan desa. Jika kita menggunakan Cablaka pada kondisi seperti ini, operasional layanan desa walaupun tetap dapat berjalan tetapi akan mengalami hambatan.

Oleh karena itu, sebelum pemasangan Cablaka ada baiknya kita mempersiapkan data-data awal yang dibutuhkan sistem. Data-data yang dibutuhkan mulai dari informasi desa, data admin pengguna Cablaka, hingga data-data warga desa yang nantinya akan memanfaatkan Cablaka.

> Anda dapat berkonsultasi perihal persiapan data awal Cablaka kepada tim kami.
